import { tablero } from "./tablero";

function recivirJugada(){
    let jugada = new Array()
    try{
        jugada ["x"] = parseInt(prompt("posicion X"));
        jugada ["y"] = parseInt(prompt("Posicion Y"));
        return jugada
    }catch(err){
        throw "datos introducidos incorrectos"; 
    }
}

function rellenaTablero(x, y, s){
    tablero [x-1][y-1] = s;
}

function compGanador (){
    let letras = new Set()
    /*Horizontal*/
    for (let i = 0; i< 3;i++){
        for (let j = 0; j< 3;j++){
            letras.add(tablero[i][j]) 
        }
        if (letras.size === 1 && !letras.has("-")){
                for (let item of letras) 
                    return item
            }    
        letras.clear()
    }
    /*Vertical*/
    
    letras.clear()
    for (let i = 0; i< 3;i++){
        for (let j = 0; j< 3;j++){
            letras.add(tablero[j][i]) 
        }
        if (letras.size === 1 && !letras.has("-")){
            for (let item of letras) 
                return item
        }    
        letras.clear()
    } 
    /*Diagonal solo dos opciones*/
    letras.add(tablero[0][0])
    letras.add(tablero[1][1])
    letras.add(tablero[2][2])
    if (letras.size === 1 && !letras.has("-")){
        for (let item of letras) 
            return item
    }
    letras.clear()
    letras.add(tablero[0][2])
    letras.add(tablero[1][1])
    letras.add(tablero[2][0])
    if (letras.size === 1 && !letras.has("-")){
        for (let item of letras) 
            return item
    }    
    return "-"
}



export {recivirJugada, rellenaTablero, compGanador}