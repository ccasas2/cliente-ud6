class Partida{
    ganador
    tablero = [["-","-","-"],["-","-","-"],["-","-","-"]]
    jugador1 = "X"
    jugador2 = "O"
    jugador_actual = this.jugador1;
    constructor(){
        this.ganador = false
    }
    cambioJugador(){
        if (this.jugador_actual === this.jugador1){
            this.jugador_actual = this.jugador2;
        }
        else{
            this.jugador_actual = this.jugador1;
        }
    }
    muestraTablero(){
        let tableroactual = ""
        for (let i = 0; i< 3;i++){
            let linea = ""
            for (let j = 0; j< 3;j++){
                linea += this.tablero[i][j] + "|";
            }
            tableroactual += linea.substring(0, linea.length -1) + "\n"
        }
        console.log(tableroactual)
    }

    recivirJugada(){
        let jugada = new Array()
        try{
            jugada ["x"] = parseInt(prompt("posicion X"));
            jugada ["y"] = parseInt(prompt("Posicion Y"));
            return jugada
        }catch(err){
            throw "datos introducidos incorrectos"; 
        }
    }

    rellenaTablero(x, y, s){
        this.tablero [x-1][y-1] = s;
    }

    compGanador (){
        let letras = new Set()
        /*Horizontal*/
        for (let i = 0; i< 3;i++){
            for (let j = 0; j< 3;j++){
                letras.add(this.tablero[i][j]) 
            }
            if (letras.size === 1 && !letras.has("-")){
                    for (let item of letras) 
                        return item
                }    
            letras.clear()
        }
        /*Vertical*/
        
        letras.clear()
        for (let i = 0; i< 3;i++){
            for (let j = 0; j< 3;j++){
                letras.add(this.tablero[j][i]) 
            }
            if (letras.size === 1 && !letras.has("-")){
                for (let item of letras) 
                    return item
            }    
            letras.clear()
        } 
        /*Diagonal solo dos opciones*/
        letras.add(this.tablero[0][0])
        letras.add(this.tablero[1][1])
        letras.add(this.tablero[2][2])
        if (letras.size === 1 && !letras.has("-")){
            for (let item of letras) 
                return item
        }
        letras.clear()
        letras.add(this.tablero[0][2])
        letras.add(this.tablero[1][1])
        letras.add(this.tablero[2][0])
        if (letras.size === 1 && !letras.has("-")){
            for (let item of letras) 
                return item
        }    
        return "-"
    }

    posicionValida(x, y){
        if (this.tablero[x-1][y-1] === "-"){
            return true
            
        }
        else {
            return false
            
        }
    }


    nuevaPartida() {
        this.ganador = false
        while (!this.ganador){
            this.muestraTablero();
            let jugada = this.recivirJugada()
            if (this.posicionValida(jugada["x"], jugada["y"])){
                this.rellenaTablero (jugada["x"],jugada["y"],this.jugador_actual)    
                let resultado = this.compGanador()
                console.log(resultado)
                if (resultado === "X" || resultado === "O"){
                    this.muestraTablero();
                    console.log("Ganador " + resultado)
                    this.ganador = true
                }
                this.cambioJugador()
            }
            else{
                console.log("posicion no valida")
            }
            
        }
    }
}


let p
p = new Partida
p.nuevaPartida()